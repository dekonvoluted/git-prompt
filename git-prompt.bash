# Generate a succinct summary string to use in a prompt

function __git_prompt_string()
{
    # Preserve exit status
    local exit=$?

    # Check if this is a git repo
    local repo_info
    local rev_parse_exit_code
    repo_info="$(git rev-parse --short HEAD 2> /dev/null)"
    rev_parse_exit_code="$?"

    # Leave if this is not a git directory
    if [ -z "${repo_info}" ]
    then
        return $exit
    fi

    local branch
    if [ "${rev_parse_exit_code}" = "0" ]
    then
        branch=${repo_info}
    fi

    # Compose the git string
    local gitstring
    gitstring="${GIT_PROMPT_FORMAT:-(%branch%)}"

    # Replace branch
    gitstring="${gitstring//\%branch\%/${branch}}"

    printf -- "%s" "${gitstring}"

    return $exit
}

