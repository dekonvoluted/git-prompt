> There is a C++ rewrite of this project in progress.
> Disregard much of the usage and implementation details below.

# Git Prompt String

A customizable git prompt for bash.
This is based on the default `git-prompt.sh` script that is released with the official git release, but differs in a few ways:

- The composition of the git prompt can be customized
- Ignored directories within a repo will still show the git prompt string
- No describe-style support for the ref
- No count support for upstream/downstream commits

# Suggested usage

1. Save this file somewhere (e.g. `~/.git-prompt.bash`).
2. Add the following line to your `~/.bashrc`:
   ```
   source ~/.git-prompt.bash
   ```
   Now, the `__git_prompt_string` function is available for use.
3. Add the following function to your `~/.bashrc`:
   ```
   function __prompt_command()
   {
       PS1='\u@\h:\w $(__git_prompt_string)\$ '
   }
   ```
   This function lets you compose your prompt string using the `__git_prompt_string` output as you like.
4. Add the following line to your `~/.bashrc`:
   ```
   PROMPT_COMMAND=__prompt_command
   ```
   This instructs bash to run the `__prompt_command` function to update your `PS1` variable.
5. Enable and customize the git prompt string.

# Customizing the git prompt string

The output of the `__git_prompt_string` function is customized entirely through environment variables.
The prompt string has two default forms, one when there is no action in progress, and one when there is an action (such as a rebase) in progress.
The composition of the prompt string in these two conditions can be specified using the following environment variables.

| Environment variable | Purpose | Default value |
| :--- | :--- | :--- |
| `GIT_PROMPT_FORMAT` | Format of the git prompt string when no action is in progress | `'(%branch%)'` |
| `GIT_PROMPT_ACTIONFORMAT` | Format of the git prompt string when an action is in progress | `'(%branch% - %action%)'` |

Use the following variables to customize these formats:

* `%branch%` - the branch/tag/SHA ref
* `%action%` - the action in progress
* `%unstaged%` - symbol to indicate the presence of unstaged changes
* `%staged%` - symbol to indicate the presence of staged changes
* `%stashed%` - symbol to indicate the presence of a stash
* `%ahead%` - symbol to indicate the local branch is ahead of its upstream
* `%behind%` - symbol to indicate the local branch is behind its upstream
* `%untracked%` - symbol to indicate the presence of untracked files

Calculating whether the local branch is ahead or behind its upstream branch only checks the locally cached remotes and will not update them.
Indicating the presence of untracked files can be quite expensive if there are a lot of temporary build files lying around.

The symbols that will replace these format variables can be specified using more environment variables.

| Environment variable | Purpose | Default value |
| :--- | :--- | :--- |
| `GIT_PROMPT_UNSTAGED_SYMBOL` | Unstaged changes | `*` |
| `GIT_PROMPT_STAGED_SYMBOL` | Staged changes | `+` |
| `GIT_PROMPT_STASHED_SYMBOL` | Stashed changes | `$` |
| `GIT_PROMPT_AHEAD_SYMBOL` | Local branch ahead of upstream | `>` |
| `GIT_PROMPT_BEHIND_SYMBOL` | Local branch behind upstream | `<` |
| `GIT_PROMPT_UNTRACKED_SYMBOL` | Untracked files | `%` |

The symbol will replace all instances of its corresponding format string in the prompt format.
It is okay to enclose the symbol within ANSI color codes to generate colorful output.
